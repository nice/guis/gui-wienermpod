<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>More HV alarms</name>
  <x>700</x>
  <y>200</y>
  <width>595</width>
  <height>665</height>
  <widget type="group" version="2.0.0">
    <name>System status of the HV board - Slot $(SLOT)</name>
    <x>10</x>
    <y>10</y>
    <width>575</width>
    <height>325</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <widget type="label" version="2.0.0">
      <name>lblModuleGoodR</name>
      <text>Module status:</text>
      <x>18</x>
      <y>30</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleSupplyGoodR</name>
      <text>Module supply status:</text>
      <x>18</x>
      <y>50</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleHWLimitVoltageGoodR</name>
      <text>HW limit voltage status:</text>
      <x>18</x>
      <y>70</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleChannelsNoErrorR</name>
      <text>Channels error status:</text>
      <x>18</x>
      <y>105</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleChannelsStableR</name>
      <text>Channels stability status:</text>
      <x>18</x>
      <y>125</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleSafetyLoopR</name>
      <text>Safety loop status:</text>
      <x>18</x>
      <y>145</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleTemperatureGoodR</name>
      <text>Temperature status:</text>
      <x>18</x>
      <y>165</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleGoodR</name>
      <pv_name>$(DEV):ModuleGood-R</pv_name>
      <x>196</x>
      <y>30</y>
      <width>313</width>
      <height>18</height>
      <off_label>Module state error</off_label>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_label>Module state is good</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleSupplyGoodR</name>
      <pv_name>$(DEV):ModuleSupplyGood-R</pv_name>
      <x>196</x>
      <y>50</y>
      <width>313</width>
      <height>18</height>
      <off_label>Power supply error</off_label>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_label>Power supply OK</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleChannelsStableR</name>
      <pv_name>$(DEV):ModuleChannelsStable-R</pv_name>
      <x>196</x>
      <y>125</y>
      <width>313</width>
      <height>18</height>
      <off_label>Channel stability error/ramping voltage</off_label>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_label>All channels stable, no ramp active</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleChannelsNoErrorR</name>
      <pv_name>$(DEV):ModuleChannelsNoError-R</pv_name>
      <x>196</x>
      <y>105</y>
      <width>313</width>
      <height>18</height>
      <off_label>Channel error</off_label>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_label>All channels without failure</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleTemperatureGoodR</name>
      <pv_name>$(DEV):ModuleTemperatureGood-R</pv_name>
      <x>196</x>
      <y>165</y>
      <width>313</width>
      <height>18</height>
      <off_label>Module temperature warning</off_label>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_label>Module temperature OK</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleHWLimitVoltageGoodR</name>
      <pv_name>$(DEV):ModuleHWLimitVoltageGood-R</pv_name>
      <x>196</x>
      <y>70</y>
      <width>313</width>
      <height>18</height>
      <off_label>Hardware limit voltage error</off_label>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_label>Hardware limit voltage in proper range</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <tooltip>$(pv_name)$(pv_value) / using for HV distributor modules with current mirror only</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleSafetyLoopR</name>
      <pv_name>$(DEV):ModuleSafetyLoop-R</pv_name>
      <x>196</x>
      <y>145</y>
      <width>313</width>
      <height>18</height>
      <off_label>Safety loop error</off_label>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_label>Safety loop closed</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtModuleTemperatureR</name>
      <pv_name>$(DEV):ModuleTemperature-R</pv_name>
      <x>196</x>
      <y>185</y>
      <width>103</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <transparent>true</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleFineAdjustmentR</name>
      <text>Module fine adjustment:</text>
      <x>18</x>
      <y>205</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleFineAdjustmentR</name>
      <pv_name>$(DEV):ModuleFineAdjustment-R</pv_name>
      <x>196</x>
      <y>205</y>
      <width>313</width>
      <height>18</height>
      <off_label>Module fine adjusting</off_label>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_label>Module has reached state fine adjustment</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleLiveInsertionR</name>
      <text>Module live insertion:</text>
      <x>18</x>
      <y>225</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleLiveInsertionR</name>
      <pv_name>$(DEV):ModuleLiveInsertion-R</pv_name>
      <x>196</x>
      <y>225</y>
      <width>313</width>
      <height>18</height>
      <off_label>Module not in live insertion</off_label>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_label>Module in state live insertion</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleNeedsServiceR</name>
      <text>Module service status:</text>
      <x>18</x>
      <y>245</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleNeedsServiceR</name>
      <pv_name>$(DEV):ModuleNeedsService-R</pv_name>
      <x>196</x>
      <y>245</y>
      <width>313</width>
      <height>18</height>
      <off_label>OK</off_label>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Hardware failure detected (consult ISEG Spezialelektronik GmbH)</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleInputErrorR</name>
      <text>Module presents input error:</text>
      <x>18</x>
      <y>265</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleInputErrorR</name>
      <pv_name>$(DEV):ModuleInputError-R</pv_name>
      <x>196</x>
      <y>265</y>
      <width>313</width>
      <height>18</height>
      <off_label>OK</off_label>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Input error in connection with module access</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleTemperatureGoodR_1</name>
      <text>Temperature</text>
      <x>18</x>
      <y>185</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>System events of HV board - Slot $(SLOT)</name>
    <x>10</x>
    <y>340</y>
    <width>575</width>
    <height>315</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventInputErrorR</name>
      <text>Input error:</text>
      <x>18</x>
      <y>175</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventSafetyLoopNotGoodR</name>
      <text>Safety loop:</text>
      <x>18</x>
      <y>195</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventTemperatureNotGoodR</name>
      <text>Temperature:</text>
      <x>18</x>
      <y>215</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventInputErrorR</name>
      <pv_name>$(DEV):ModuleEventInputError-R</pv_name>
      <x>196</x>
      <y>175</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Input error in connection with module access</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventTemperatureNotGoodR</name>
      <pv_name>$(DEV):ModuleEventTemperatureNotGood-R</pv_name>
      <x>196</x>
      <y>215</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Temperature above permitted threshold</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventSafetyLoopNotGoodR</name>
      <pv_name>$(DEV):ModuleEventSafetyLoopNotGood-R</pv_name>
      <x>196</x>
      <y>195</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Safety loop open</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventPowerFailR</name>
      <text>Power fail:</text>
      <x>18</x>
      <y>50</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventSupplyNotGoodR</name>
      <text>Module supply status:</text>
      <x>18</x>
      <y>85</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventHWVoltageLimitNotGoodR</name>
      <text>HW limit voltage:</text>
      <x>18</x>
      <y>105</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventLiveInsertionR</name>
      <text>Live insertion:</text>
      <x>18</x>
      <y>30</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventPowerFailR</name>
      <pv_name>$(DEV):ModuleEventPowerFail-R</pv_name>
      <x>196</x>
      <y>50</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Power fail generated by MPOD Controller to ramp down all HV (1000 Vps)</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventLiveInsertionR</name>
      <pv_name>$(DEV):ModuleEventLiveInsertion-R</pv_name>
      <x>196</x>
      <y>30</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-BLUE-OFF" red="90" green="110" blue="110">
        </color>
      </off_color>
      <on_label>Live insertion to prepare hot plug of module</on_label>
      <on_color>
        <color name="LED-BLUE-ON" red="81" green="232" blue="255">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventHWVoltageLimitNotGoodR</name>
      <pv_name>$(DEV):ModuleEventHWVoltageLimitNotGood-R</pv_name>
      <x>196</x>
      <y>105</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Hardware voltage limit out of range, using for HV distributor modules with current mirror only</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventSupplyNotGoodR</name>
      <pv_name>$(DEV):ModuleEventSupplyNotGood-R</pv_name>
      <x>196</x>
      <y>85</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>1 or more supply failure</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>btnModuleClearEventsS</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(DEV):ModuleClearEvents-S</pv_name>
      <text>Clear events</text>
      <x>443</x>
      <y>252</y>
      <width>90</width>
      <height>20</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <background_color>
        <color name="Read_Background" red="230" green="235" blue="232">
        </color>
      </background_color>
      <tooltip>$(pv_name)
$(pv_value)</tooltip>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledModuleEventServiceR</name>
      <pv_name>$(DEV):ModuleEventService-R</pv_name>
      <x>196</x>
      <y>140</y>
      <width>313</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Hardware failure, contact iseg Spezialelektronik GmbH</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblModuleEventServiceR</name>
      <text>Service status:</text>
      <x>18</x>
      <y>140</y>
      <width>170</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
  </widget>
</display>
