# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Wiener MPOD interface; procedure to parse seconds up and running into months,
#     days, hours, minutes...
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# DET  - anders.lindholsson@esss.se
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
#from pvaccess import BOOLEAN, BYTE, UBYTE, SHORT, USHORT, INT, UINT, LONG, ULONG, FLOAT, DOUBLE, STRING, PvObject, PvaServer

import sys, time, datetime

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    try:
        # -------------------------------------------------------------------------
        # logical representation of PVs
        # -------------------------------------------------------------------------
        upSeconds = PVUtil.getDouble(pvs[0])        # $(DEV):OperatingTimeR
        upTime    = datetime.timedelta(seconds=upSeconds)

        pvs[1].setValue("{} month(s), {} day(s)\n{} (h:mm:ss)".format(upTime.days//30, upTime.days%30, datetime.timedelta(seconds=upTime.seconds)))
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()