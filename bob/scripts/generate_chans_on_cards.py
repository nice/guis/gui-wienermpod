# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Wiener MPOD interface; procedure to parse description obtained by SNMP
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# DET  - anders.lindholsson@esss.se
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.model.properties import WidgetColor, WidgetFont, WidgetFontStyle

import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Wiener MPOD has 10 slots for HV/LV boards
# -----------------------------------------------------------------------------
MAX_SLOTS = 10

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    # -----------------------------------------------------------------------------
    # This Python script is attached to a display
    # and triggered by loc://initial_trigger2$(DID)(1)
    # to execute once when the display is loaded.
    # -----------------------------------------------------------------------------
    try:
        # -----------------------------------------------------------------------------
        # Input PVs
        # -----------------------------------------------------------------------------
        moduleDesc = []
        moduleDesc0 = PVUtil.getString(pvs[1])
        moduleDesc.append(moduleDesc0)
        moduleDesc1 = PVUtil.getString(pvs[2])
        moduleDesc.append(moduleDesc1)
        moduleDesc2 = PVUtil.getString(pvs[3])
        moduleDesc.append(moduleDesc2)
        moduleDesc3 = PVUtil.getString(pvs[4])
        moduleDesc.append(moduleDesc3)
        moduleDesc4 = PVUtil.getString(pvs[5])
        moduleDesc.append(moduleDesc4)
        moduleDesc5 = PVUtil.getString(pvs[6])
        moduleDesc.append(moduleDesc5)
        moduleDesc6 = PVUtil.getString(pvs[7])
        moduleDesc.append(moduleDesc6)
        moduleDesc7 = PVUtil.getString(pvs[8])
        moduleDesc.append(moduleDesc7)
        moduleDesc8 = PVUtil.getString(pvs[9])
        moduleDesc.append(moduleDesc8)
        moduleDesc9 = PVUtil.getString(pvs[10])
        moduleDesc.append(moduleDesc9)
        # Type of the card (HV/LV)
        cartdType   = PVUtil.getString(pvs[11])

        # -----------------------------------------------------------------------------
        # Loading number of channels for installed modules from PVs
        # -----------------------------------------------------------------------------
        display         = widget.getDisplayModel()
        old_macros      = display.getEffectiveMacros()
        card_nr         = old_macros.getValue('SLOT')
        nbrOfchannels   = int(moduleDesc[int(card_nr)].split(',')[2])
        #logger.info("moduleChan: %d" % int(moduleDesc[int(card_nr)].split(',')[2]))

        channels = []
        for i in range(nbrOfchannels):
            channels.append({
                            'NAME' : "Channel %d" % (i),
                            'CHAN' : "0%d" % (i) if (i) < 10 else "%d" % (i)
                            })

        # -----------------------------------------------------------------------------
        # Create display:
        # -----------------------------------------------------------------------------
        embedded_width  = 10
        embedded_height = 10
        label_pos_x = 0
        label_pos_y = 50
        label_width  = 60
        label_height = 20

        def createStatusLabelInstance():
            label = WidgetFactory.getInstance().getWidgetDescriptor("label").createWidget()
            label.setPropertyValue("name", "lblAlarms")
            label.setPropertyValue("text", "Channels")
            label.setPropertyValue("x", label_pos_x)
            label.setPropertyValue("y", label_pos_y)
            label.setPropertyValue("width", label_width)
            label.setPropertyValue("height", label_height)
            label.setPropertyValue("horizontal_alignment", 1)       # Center
            label.setPropertyValue("vertical_alignment", 1)         # Middle
            # public WidgetFont(final String family, final WidgetFontStyle style, final double size)
            label_font = WidgetFont("Source Sans Pro", WidgetFontStyle.BOLD, 12.0)       # font family="Source Sans Pro" style="BOLD" size="12.0"
            label.setPropertyValue("font", label_font)
            # public WidgetColor(final int red, final int green, final int blue, final int alpha)
            back_color = WidgetColor(200, 205, 201)     # GROUP-BACKGROUND; red="200" green="205" blue="201"
            label.setPropertyValue("background_color", back_color)
            return label

        def createStatusInstance(x, y, macros):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", embedded_width)
            embedded.setPropertyValue("height", embedded_height)
            embedded.setPropertyValue("resize", "2")
            for macro, value in macros.items():
                embedded.getPropertyValue("macros").add(macro, value)
            embedded.setPropertyValue("file", "channel_status.bob")
            return embedded

        def createAlarmInstance(x, y, macros=[]):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", 58)
            embedded.setPropertyValue("height", 50)
            if cartdType == "HV":
                embedded.setPropertyValue("file", "HV/single_hv_alarms.bob")
            #elif cartdType == "LV":
            #    embedded.setPropertyValue("file", "card_lv_alarms.bob")
            return embedded

        # ---------------------------------------------------------------------
        # Creating the label to inform the status is related to "Ćhannels"
        # ---------------------------------------------------------------------
        instance = createStatusLabelInstance()
        display.runtimeChildren().addChild(instance)

        # ---------------------------------------------------------------------
        # Creating the LED components for channel status (On/Off)
        # ---------------------------------------------------------------------
        startX   = 10
        startY   = 70
        columns  = 4

        for i in range(len(channels)):
            x = startX + embedded_width * (i % columns)
            y = startY + embedded_height * (i / columns)
            instance = createStatusInstance(x, y, channels[i])
            display.runtimeChildren().addChild(instance)

        # ---------------------------------------------------------------------
        # Creating the LED components for board alarms
        # ---------------------------------------------------------------------
        startX   = 1
        #logger.info("cartdType: %s" % str(cartdType))
        if cartdType == "HV":
            # only HV board has such useful information
            #logger.info("creating alarm display...")
            span_between = embedded_height * ((len(channels) // columns) + 1)        # use the number of rows adding 1 as a space
            instance = createAlarmInstance(startX, startY + span_between)
            display.runtimeChildren().addChild(instance)

    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()